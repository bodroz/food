import {autoinject, PLATFORM} from "aurelia-framework"
import{RouterConfiguration, Router} from'aurelia-router';
import { AppState } from './state/app-state';
import JwtDecode from "jwt-decode";
import {Redirect} from 'aurelia-router';
import {
    NavigationInstruction,
    Next,
    PipelineStep,
} from 'aurelia-router';

@autoinject

export class App {
    router?: Router;
    constructor(private appState: AppState) {

    }

    configureRouter(config: RouterConfiguration, router: Router): void{
        this.router = router;

        config.addAuthorizeStep(AuthorizeStep);
        config.title = "Food";
        config.map([
            {route: ['','home','home/index'], name: 'home', moduleId: PLATFORM.moduleName('views/home/index'), nav: true, title: 'Home', settings: { roles: [] }},

            { route: ['account/login'], name: 'account-login', moduleId: PLATFORM.moduleName('views/account/login'), nav: false, title: 'login' , settings: { roles: [] }},
            { route: ['account/register'], name: 'account-register', moduleId: PLATFORM.moduleName('views/account/register'), nav: false, title: 'register' , settings: { roles: [] }},

            { route: ['drinks', 'drinks/index'], name: 'drinks-index', moduleId: PLATFORM.moduleName('views/drinks/index'), nav: true, title: 'drinks', settings: { roles: [] } },
            { route: ['drinks/details/:id?'], name: 'drinks-details', moduleId: PLATFORM.moduleName('views/drinks/details'), nav: false, title: 'drinks Details', settings: { roles: ['admin'] } },
            { route: ['drinks/edit/:id?'], name: 'drinks-edit', moduleId: PLATFORM.moduleName('views/drinks/edit'), nav: false, title: 'drinks Edit', settings: { roles: ['admin'] } },
            { route: ['drinks/delete/:id?'], name: 'drinks-delete', moduleId: PLATFORM.moduleName('views/drinks/delete'), nav: false, title: 'drinks Delete', settings: { roles: ['admin'] } },
            { route: ['drinks/create'], name: 'drinks-create', moduleId: PLATFORM.moduleName('views/drinks/create'), nav: false, title: 'drinks Create', settings: { roles: ['admin'] } },

            { route: ['food', 'food/index'], name: 'food-index', moduleId: PLATFORM.moduleName('views/foods/index'), nav: true, title: 'food', settings: { roles: [] } },
            { route: ['food/details/:id?'], name: 'food-details', moduleId: PLATFORM.moduleName('views/foods/details'), nav: false, title: 'food Details', settings: { roles: ['admin'] } },
            { route: ['food/edit/:id?'], name: 'food-edit', moduleId: PLATFORM.moduleName('views/foods/edit'), nav: false, title: 'food Edit', settings: { roles: ['admin'] } },
            { route: ['food/delete/:id?'], name: 'food-delete', moduleId: PLATFORM.moduleName('views/foods/delete'), nav: false, title: 'food Delete', settings: { roles: ['admin'] } },
            { route: ['food/create'], name: 'food-create', moduleId: PLATFORM.moduleName('views/foods/create'), nav: false, title: 'food Create', settings: { roles: ['admin'] } },

            { route: ['drinkCategories', 'drinkCategories/index'], name: 'drinkCategories-index', moduleId: PLATFORM.moduleName('views/drinkCategories/index'), nav: true, title: 'drinkCategories', settings: { roles: [] } },
            { route: ['drinkCategories/details/:id?'], name: 'drinkCategories-details', moduleId: PLATFORM.moduleName('views/drinkCategories/details'), nav: false, title: 'drinkCategories Details', settings: { roles: ['admin'] } },
            { route: ['drinkCategories/edit/:id?'], name: 'drinkCategories-edit', moduleId: PLATFORM.moduleName('views/drinkCategories/edit'), nav: false, title: 'drinkCategories Edit', settings: { roles: ['admin'] } },
            { route: ['drinkCategories/delete/:id?'], name: 'drinkCategories-delete', moduleId: PLATFORM.moduleName('views/drinkCategories/delete'), nav: false, title: 'drinkCategories Delete', settings: { roles: ['admin'] } },
            { route: ['drinkCategories/create'], name: 'drinkCategories-create', moduleId: PLATFORM.moduleName('views/drinkCategories/create'), nav: false, title: 'drinkCategories Create', settings: { roles: ['admin'] } },

            { route: ['foodCategories', 'foodCategories/index'], name: 'foodCategories-index', moduleId: PLATFORM.moduleName('views/foodCategories/index'), nav: true, title: 'foodCategories', settings: { roles: [] } },
            { route: ['foodCategories/details/:id?'], name: 'foodCategories-details', moduleId: PLATFORM.moduleName('views/foodCategories/details'), nav: false, title: 'foodCategories Details', settings: { roles: ['admin'] } },
            { route: ['foodCategories/edit/:id?'], name: 'foodCategories-edit', moduleId: PLATFORM.moduleName('views/foodCategories/edit'), nav: false, title: 'foodCategories Edit', settings: { roles: ['admin'] } },
            { route: ['foodCategories/delete/:id?'], name: 'foodCategories-delete', moduleId: PLATFORM.moduleName('views/foodCategories/delete'), nav: false, title: 'foodCategories Delete', settings: { roles: ['admin'] } },
            { route: ['foodCategories/create'], name: 'foodCategories-create', moduleId: PLATFORM.moduleName('views/foodCategories/create'), nav: false, title: 'foodCategories Create', settings: { roles: ['admin'] } },

            { route: ['restaurants', 'restaurants/index'], name: 'restaurants-index', moduleId: PLATFORM.moduleName('views/restaurants/index'), nav: true, title: 'restaurants', settings: { roles: [] } },
            { route: ['restaurants/details/:id?'], name: 'restaurants-details', moduleId: PLATFORM.moduleName('views/restaurants/details'), nav: false, title: 'restaurants Details', settings: { roles: ['admin'] } },
            { route: ['restaurants/edit/:id?'], name: 'restaurants-edit', moduleId: PLATFORM.moduleName('views/restaurants/edit'), nav: false, title: 'restaurants Edit', settings: { roles: ['admin'] } },
            { route: ['restaurants/delete/:id?'], name: 'restaurants-delete', moduleId: PLATFORM.moduleName('views/restaurants/delete'), nav: false, title: 'restaurants Delete', settings: { roles: ['admin'] } },
            { route: ['restaurants/create'], name: 'restaurants-create', moduleId: PLATFORM.moduleName('views/restaurants/create'), nav: false, title: 'restaurants Create', settings: { roles: ['admin'] } },

        ]);

        config.mapUnknownRoutes('views/home/index')
    }

    logoutOnClick(){
        this.appState.jwt = null;
        this.router!.navigateToRoute('account-login');
    }

}

@autoinject
class AuthorizeStep implements PipelineStep {
    constructor(private appState: AppState) {

    }
    public checkIsAdmin(){
        if (this.appState.jwt !== null) {
            const decoded = JwtDecode(this.appState.jwt) as Record<string, string[]>;
            if (decoded["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"].includes("Admin")) {
                return true
            }
        }
        return false;
    }
    public run(navigationInstruction: NavigationInstruction, next: Next): Promise<any> {
        if (navigationInstruction.getAllInstructions().some(i => i.config.settings.roles.indexOf('admin') !== -1)) {
            var isAdmin = this.checkIsAdmin();
            if (!isAdmin) {
                return next.cancel(new Redirect(''));
            }
        }
        return next();
    }
}

