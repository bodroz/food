export interface IFoodCreate {
    name: string,
    price: string,
    foodCategoryId: string,
    restaurantId: string
}
