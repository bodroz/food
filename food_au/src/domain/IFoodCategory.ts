import {IFoodCategoryCreate} from "./IFoodCategoryCreate";

export interface IFoodCategory extends IFoodCategoryCreate{
    id: string;
}
