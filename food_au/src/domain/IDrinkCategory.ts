import {IDrinkCategoryCreate} from "./IDrinkCategoryCreate";

export interface IDrinkCategory extends IDrinkCategoryCreate{
    id: string;
}
