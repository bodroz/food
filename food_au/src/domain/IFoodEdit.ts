import {IFoodCreate} from "./IFoodCreate";

export interface IFoodEdit extends IFoodCreate{
    id: string;
}
