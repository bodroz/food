import {IRestaurantCreate} from "./IRestaurantCreate";

export interface IRestaurantEdit extends IRestaurantCreate{
    id: string;
}
