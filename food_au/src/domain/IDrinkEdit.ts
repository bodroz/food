import {IDrinkCreate} from "./IDrinkCreate";

export interface IDrinkEdit extends IDrinkCreate{
    id: string;
}
