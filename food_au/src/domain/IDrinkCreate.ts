export interface IDrinkCreate {
    name: string,
    price: string,
    drinkCategoryId: string,
    restaurantId: string
}
