import {IRestaurant} from "./IRestaurant";
import {IDrinkCategory} from "./IDrinkCategory";
import {IDrinkCreate} from "./IDrinkCreate";

export interface IDrink extends IDrinkCreate{
    id: string;
    drinkCategory: IDrinkCategory,
    restaurant: IRestaurant
}
