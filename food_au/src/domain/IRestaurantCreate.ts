export interface IRestaurantCreate  {
    name:string;
    Description:string;
    Telephone:string;
    Address:string;
    image:string;
}
