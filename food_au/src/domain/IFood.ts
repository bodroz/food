import {IFoodCreate} from "./IFoodCreate";
import {IFoodCategory} from "./IFoodCategory";
import {IRestaurant} from "./IRestaurant";

export interface IFood extends IFoodCreate{
    id: string;
    foodCategory: IFoodCategory,
    restaurant: IRestaurant
}
