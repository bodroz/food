import {IRestaurantCreate} from "./IRestaurantCreate";

export interface IRestaurant extends IRestaurantCreate{
    id: string;
    ReviewCount:string;
}
