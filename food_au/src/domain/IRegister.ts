export interface IRegister {
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    phoneNumber: string,
    birthday: Date
}
