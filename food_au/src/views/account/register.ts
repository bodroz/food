import { Router } from 'aurelia-router';
import { AppState } from '../../state/app-state';
import { autoinject } from 'aurelia-framework';
import { AccountService } from 'services/account-service';
import {IRegister} from "../../domain/IRegister";

@autoinject
export class AccountRegister {

    private _credentials: IRegister = {
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        phoneNumber: '',
        birthday: new Date()
    };
    private _errorMessage: string | null = null;

    constructor(private accountService: AccountService, private appState: AppState, private router: Router) {

    }

    onSubmit(event: Event) {
        event.preventDefault();

        this.accountService.register(this._credentials).then(
            response => {
                if (response.statusCode == 200) {
                    this.router!.navigateToRoute('home');
                } else {
                    this._errorMessage = response.statusCode.toString()
                        + ' '
                        + response.errorMessage!
                }
            }
        );
    }

}
