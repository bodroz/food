import { autoinject } from 'aurelia-framework';
import { RouteConfig, NavigationInstruction } from 'aurelia-router';
import { IAlertData } from 'types/IAlertData';
import { AlertType } from 'types/AlertType';
import {IDrinkCategory} from "../../domain/IDrinkCategory";
import {DrinkCategoriesService} from "../../services/drink-categories-service";

@autoinject
export class DrinkCategoryDetails {

    private _category?: IDrinkCategory;
    private _alert: IAlertData | null = null;


    constructor(private drinkCategoriesService: DrinkCategoriesService) {

    }

    attached() {
    }

    activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {
            this.drinkCategoriesService.getDrinkCategory(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this._category = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        };
                        this._category = undefined;
                    }
                }
            );
        }
    }

}
