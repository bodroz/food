import { AlertType } from '../../types/AlertType';
import { autoinject } from 'aurelia-framework';
import { IAlertData } from 'types/IAlertData';
import {IDrinkCategory} from "../../domain/IDrinkCategory";
import {DrinkCategoriesService} from "../../services/drink-categories-service";

@autoinject
export class FoodCategoryIndex {
    private _drinkCategories: IDrinkCategory[] = [];

    private _alert: IAlertData | null = null;

    constructor(private drinkCategoriesService: DrinkCategoriesService) {

    }


    attached() {
        this.drinkCategoriesService.getDrinkCategories().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._drinkCategories = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
    }

}
