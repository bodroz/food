import { AlertType } from '../../types/AlertType';
import { IRestaurant } from '../../domain/IRestaurant';
import { autoinject } from 'aurelia-framework';
import { RestaurantService } from 'services/restaurant-service';
import { IAlertData } from 'types/IAlertData';
import {FoodCategoriesService} from "../../services/food-categories-service";
import {IFoodCategory} from "../../domain/IFoodCategory";
import {NavigationInstruction, RouteConfig, Router} from "aurelia-router";
import {FoodService} from "../../services/food-service";
import {IDrinkCategory} from "../../domain/IDrinkCategory";
import {DrinkCategoriesService} from "../../services/drink-categories-service";
import {DrinkService} from "../../services/drink-service";

@autoinject
export class DrinkCreate {


    private _restaurants: IRestaurant[] = [];

    private _drinkCategories: IDrinkCategory[] = [];

    private _alert: IAlertData | null = null;

    _name = ''
    _price = ''
    _category = {} as IDrinkCategory
    _restaurant = {} as IRestaurant

    constructor(private restaurantService: RestaurantService, private drinkCategoriesService: DrinkCategoriesService,
                private router: Router, private drinkService: DrinkService) {

    }


    attached() {
        this.drinkCategoriesService.getDrinkCategories().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._drinkCategories = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
        this.restaurantService.getRestaurants().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._restaurants = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
    }
    activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {

    }

    onSubmit(event: Event) {
        console.log(this._restaurant)
        this.drinkService
            .createDrink({ name: this._name,price : this._price , drinkCategoryId: this._category.id,restaurantId : this._restaurant.id})
            .then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('drinks-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }

}
