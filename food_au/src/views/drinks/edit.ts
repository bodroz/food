import { AlertType } from '../../types/AlertType';
import { IRestaurant } from '../../domain/IRestaurant';
import { autoinject } from 'aurelia-framework';
import { RestaurantService } from 'services/restaurant-service';
import { IAlertData } from 'types/IAlertData';
import {NavigationInstruction, RouteConfig, Router} from "aurelia-router";
import {IDrinkCategory} from "../../domain/IDrinkCategory";
import {IDrinkEdit} from "../../domain/IDrinkEdit";
import {DrinkCategoriesService} from "../../services/drink-categories-service";
import {DrinkService} from "../../services/drink-service";

@autoinject
export class DrinkEdit {


    private _restaurants: IRestaurant[] = [];

    private _drinkCategories: IDrinkCategory[] = [];

    private _alert: IAlertData | null = null;

    private _drink?: IDrinkEdit;

    constructor(private restaurantService: RestaurantService, private drinkCategoriesService: DrinkCategoriesService,
                private router: Router, private drinkService: DrinkService) {

    }


    attached() {
        this.drinkCategoriesService.getDrinkCategories().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._drinkCategories = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
        this.restaurantService.getRestaurants().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._restaurants = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
    }
    activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {
            this.drinkService.getDrink(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this._drink = {id : response.data!.id,
                            name: response.data!.name,
                            price: response.data!.price.replace(/[^0-9,.-]+/g, "").replace(',', '.'),
                            drinkCategoryId: response.data!.drinkCategory.id,
                            restaurantId: response.data!.restaurant.id
                        } as IDrinkEdit
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        }
    }

    onSubmit(event: Event) {
        this.drinkService
            .updateDrink(this._drink!)
            .then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('drinks-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }

}
