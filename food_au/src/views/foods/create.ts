import { AlertType } from '../../types/AlertType';
import { IRestaurant } from '../../domain/IRestaurant';
import { autoinject } from 'aurelia-framework';
import { RestaurantService } from 'services/restaurant-service';
import { IAlertData } from 'types/IAlertData';
import {FoodCategoriesService} from "../../services/food-categories-service";
import {IFoodCategory} from "../../domain/IFoodCategory";
import {NavigationInstruction, RouteConfig, Router} from "aurelia-router";
import {FoodService} from "../../services/food-service";

@autoinject
export class FoodCreate {


    private _restaurants: IRestaurant[] = [];

    private _foodCategories: IFoodCategory[] = [];

    private _alert: IAlertData | null = null;

    _name = ''
    _price = ''
    _category = {} as IFoodCategory
    _restaurant = {} as IRestaurant

    constructor(private restaurantService: RestaurantService, private foodCategoryService: FoodCategoriesService,
                private router: Router, private foodService: FoodService) {

    }


    attached() {
        this.foodCategoryService.getFoodCategories().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._foodCategories = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
        this.restaurantService.getRestaurants().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._restaurants = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
    }
    activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {

    }

    onSubmit(event: Event) {
        console.log(this._restaurant)
        this.foodService
            .createFood({ name: this._name,price : this._price , foodCategoryId: this._category.id,restaurantId : this._restaurant.id})
            .then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('food-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }

}
