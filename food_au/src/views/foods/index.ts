import { AlertType } from '../../types/AlertType';
import { IRestaurant } from '../../domain/IRestaurant';
import { autoinject } from 'aurelia-framework';
import { RestaurantService } from 'services/restaurant-service';
import { IAlertData } from 'types/IAlertData';
import {IFood} from "../../domain/IFood";
import {FoodService} from "../../services/food-service";

@autoinject
export class foodIndex {
    private _foods: IFood[] = [];

    private _alert: IAlertData | null = null;

    constructor(private foodService: FoodService) {

    }


    attached() {
        this.foodService.getFoods().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._foods = response.data!;
                    console.log(response.data!)
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
    }

}


