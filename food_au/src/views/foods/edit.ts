import { AlertType } from '../../types/AlertType';
import { IRestaurant } from '../../domain/IRestaurant';
import { autoinject } from 'aurelia-framework';
import { RestaurantService } from 'services/restaurant-service';
import { IAlertData } from 'types/IAlertData';
import {FoodCategoriesService} from "../../services/food-categories-service";
import {IFoodCategory} from "../../domain/IFoodCategory";
import {NavigationInstruction, RouteConfig, Router} from "aurelia-router";
import {FoodService} from "../../services/food-service";
import {IFoodEdit} from "../../domain/IFoodEdit";

@autoinject
export class FoodEdit {


    private _restaurants: IRestaurant[] = [];

    private _foodCategories: IFoodCategory[] = [];

    private _alert: IAlertData | null = null;

    private _food?: IFoodEdit;

    constructor(private restaurantService: RestaurantService, private foodCategoryService: FoodCategoriesService,
                private router: Router, private foodService: FoodService) {

    }


    attached() {
        this.foodCategoryService.getFoodCategories().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._foodCategories = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
        this.restaurantService.getRestaurants().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._restaurants = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
    }
    activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {
            this.foodService.getFood(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this._food = {id : response.data!.id,
                            name: response.data!.name,
                            price: response.data!.price.replace(/[^0-9,.-]+/g, "").replace(',', '.'),
                            foodCategoryId: response.data!.foodCategory.id,
                            restaurantId: response.data!.restaurant.id
                        } as IFoodEdit
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        }
    }

    onSubmit(event: Event) {
        this.foodService
            .updateFood(this._food!)
            .then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('food-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );

        event.preventDefault();
    }

}
