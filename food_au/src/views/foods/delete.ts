import { autoinject } from 'aurelia-framework';
import { RouteConfig, NavigationInstruction, Router } from 'aurelia-router';
import { RestaurantService } from 'services/restaurant-service';
import { IAlertData } from 'types/IAlertData';
import { AlertType } from 'types/AlertType';
import {IFood} from "../../domain/IFood";
import {FoodService} from "../../services/food-service";

@autoinject
export class FoodDelete {
    private _alert: IAlertData | null = null;

    private _food?: IFood;

    constructor(private foodService: FoodService, private router: Router) {

    }

    attached() {

    }

    activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        if (params.id && typeof (params.id) == 'string') {
            this.foodService.getFood(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this._food = response.data!;
                        console.log(this._food)
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        };
                        this._food = undefined;
                    }
                }
            );
        }
    }

    onSubmit(event: Event) {
        console.log(this._food!)
        this.foodService
            .deleteFood(this._food!.id)
            .then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this.router.navigateToRoute('food-index', {});
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        }
                    }
                }
            );
        event.preventDefault();
    }
}
