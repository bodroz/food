import { AlertType } from '../../types/AlertType';
import { autoinject } from 'aurelia-framework';
import { IAlertData } from 'types/IAlertData';
import {IFoodCategory} from "../../domain/IFoodCategory";
import {FoodCategoriesService} from "../../services/food-categories-service";

@autoinject
export class FoodCategoryIndex {
    private _foodCategories: IFoodCategory[] = [];

    private _alert: IAlertData | null = null;

    constructor(private foodCategoryService: FoodCategoriesService) {

    }


    attached() {
        this.foodCategoryService.getFoodCategories().then(
            response => {
                if (response.statusCode >= 200 && response.statusCode < 300) {
                    this._alert = null;
                    this._foodCategories = response.data!;
                } else {
                    this._alert = {
                        message: response.statusCode.toString() + ' - ' + response.errorMessage,
                        type: AlertType.Danger,
                        dismissable: true,
                    }
                }
            }
        );
    }

}
