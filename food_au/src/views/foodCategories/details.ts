import { autoinject } from 'aurelia-framework';
import { RouteConfig, NavigationInstruction } from 'aurelia-router';
import { IAlertData } from 'types/IAlertData';
import { AlertType } from 'types/AlertType';
import {FoodCategoriesService} from "../../services/food-categories-service";
import {IFoodCategory} from "../../domain/IFoodCategory";

@autoinject
export class FoodCategoryDetails {

    private _category?: IFoodCategory;
    private _alert: IAlertData | null = null;


    constructor(private foodCategoryService: FoodCategoriesService) {

    }

    attached() {
    }

    activate(params: any, routeConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
        console.log(params);
        if (params.id && typeof (params.id) == 'string') {
            this.foodCategoryService.getFoodCategory(params.id).then(
                response => {
                    if (response.statusCode >= 200 && response.statusCode < 300) {
                        this._alert = null;
                        this._category = response.data!;
                    } else {
                        // show error message
                        this._alert = {
                            message: response.statusCode.toString() + ' - ' + response.errorMessage,
                            type: AlertType.Danger,
                            dismissable: true,
                        };
                        this._category = undefined;
                    }
                }
            );
        }
    }

}
