import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Login from '@/views/Account/Login.vue'
import RestaurantsIndex from "@/views/Restaurants/index.vue";
import store from '@/store/index'
import axios from 'axios'
import ReviewsIndex from "@/views/Restaurants/reviews.vue";
import DeliveryIndex from "@/views/Delivery/index.vue";
import JwtDecode from "jwt-decode";
import OrdersIndex from "@/views/Orders/index.vue";
Vue.use(VueRouter)

const isAdmin = (to: any, from: any, next: any) => {
    if (store.state.jwt) {
        const decoded = JwtDecode(store.state.jwt) as Record<string, string[]>;
        if (!decoded["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"].includes("Admin")) {
            return next("/restaurants")
        }
        return next()
    }
    return next("/restaurants")
}

const routes: Array<RouteConfig> = [
    { path: '/account/register', name: 'register', component: Login, meta: { disallowAuthed: true } },
    { path: '/', name: 'restaurants', component: RestaurantsIndex },
    { path: '/orders', name: 'orders', component: OrdersIndex, meta: { authRequired: true } },
    { path: '/restaurant/:id?', name: 'reviews', component: ReviewsIndex },
    { path: '/deliveries', name: 'deliveries', component: DeliveryIndex, meta: { authRequired: true }, beforeEnter: isAdmin }
]

const router = new VueRouter({
    mode: 'history',
    routes
})
// get userEmail(): string {
//     if (store.state.jwt) {
//         const decoded = JwtDecode(store.state.jwt) as Record<string, string>;
//         return decoded["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"];
//     }
//     return 'null';
// }

router.beforeEach((to, from, next) => {
    const loggedIn: boolean = store.getters.isAuthenticated;

    if (!loggedIn && to.meta.authRequired) {
        return router.push({ name: 'register' });
    }
    if (loggedIn) {
        if (to.meta.authRequired) {
            axios.defaults.headers.common.Authorization = 'Bearer ' + store.getters.getJwt;
        }
        if (to.meta.disallowAuthed) {
            return router.push({ name: 'restaurants' });
        }
    }
    return next();
});

export default router
