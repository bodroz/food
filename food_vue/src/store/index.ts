import Vue from 'vue'
import Vuex from 'vuex'
import { ILoginDTO } from "@/types/ILoginDTO";
import { AccountApi } from "@/services/AccountApi";
import { IRegisterDTO } from "@/types/IRegisterDTO";
import { IRestaurantDTO } from "@/domain/IRestaurantDTO";
import { RestaurantApi } from "@/services/RestaurantApi";
import axios from "axios";
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        jwt: localStorage.getItem('jwt') || '',
        restaurants: [] as IRestaurantDTO[]

    },
    mutations: {

        setJwt(state, jwt: string) {
            state.jwt = jwt;
        },
        setRestaurants(state, restaurants: IRestaurantDTO[]) {
            state.restaurants = restaurants;
        }

    },
    getters: {
        getJwt(context): string {
            return context.jwt!;
        },
        isAuthenticated(context): boolean {
            return !!context.jwt
        }
    },
    actions: {
        clearJwt(context): void {
            localStorage.removeItem('jwt')
            context.commit('setJwt', '');
            delete axios.defaults.headers.common.Authorization
        },
        async registerUser(context, registerDTO: IRegisterDTO) {
            await AccountApi.register(registerDTO);
        },
        async authenticateUser(context, loginDTO: ILoginDTO): Promise<boolean> {
            return new Promise((resolve, reject) => {
                AccountApi.getJwt(loginDTO)
                    .then(resp => {
                        context.commit('setJwt', resp.data.token)
                        localStorage.setItem('jwt', resp.data.token)
                        resolve()
                    })
                    .catch(error => {
                        localStorage.removeItem('jwt')
                        reject(error)
                    });
            });
        },
        async getRestaurants(context): Promise<void> {
            const restaurants = await RestaurantApi.getAll();
            context.commit('setRestaurants', restaurants);
        }
    },
    modules: {}
})
