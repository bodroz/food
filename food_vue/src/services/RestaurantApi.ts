import Axios from 'axios';
import { IRestaurantDTO } from "@/domain/IRestaurantDTO";
import { IRestaurantReviewDTO } from "@/domain/IRestaurantReviewDTO";

export abstract class RestaurantApi {
    private static axios = Axios.create(
        {
            baseURL: "https://bodroz-food.azurewebsites.net/api/v1.0/Restaurants/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll() {
        return this.axios.get<IRestaurantDTO[]>('').then(response => {
            return response.data;
        })
    }

    static async get(id: string) {
        return this.axios.get<IRestaurantReviewDTO>(id).then(response => {
            return response.data;
        })
    }
}
