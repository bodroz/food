import Axios from "axios";
import { IReviewDTO } from "@/domain/IReviewDTO";

export abstract class ReviewApi {
    private static axios = Axios.create(
        {
            baseURL: "https://bodroz-food.azurewebsites.net/api/v1.0/Reviews/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getUserReviews() {
        return this.axios.get<IReviewDTO[]>('', { headers: { Authorization: 'Bearer ' + localStorage.getItem('jwt') } }).then(response => {
            return response.data;
        })
    }

    static async delete(id: string) {
        await this.axios.delete(id, { headers: { Authorization: 'Bearer ' + localStorage.getItem('jwt') } });
    }

    static async create(reviewDTO: IReviewDTO) {
        await this.axios.post('', reviewDTO, { headers: { Authorization: 'Bearer ' + localStorage.getItem('jwt') } });
    }
}
