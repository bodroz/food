import { ILoginDTO } from '@/types/ILoginDTO';
import Axios, { AxiosResponse } from 'axios';
import { IRegisterDTO } from "@/types/IRegisterDTO";

interface ILoginResponse {
    token: string;
    status: string;
}

export abstract class AccountApi {
    private static axios = Axios.create(
        {
            baseURL: "https://bodroz-food.azurewebsites.net/api/v1.0/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async register(registerDTO: IRegisterDTO) {
        await this.axios.post('account/register', registerDTO);
    }

    static async getJwt(loginDTO: ILoginDTO): Promise<AxiosResponse<ILoginResponse>> {
           return this.axios.post('account/login', loginDTO);
    }
}
