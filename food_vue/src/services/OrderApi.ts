import Axios from "axios";
import { IOrderInfoDTO } from "@/domain/IOrderInfoDTO";
import { IOrderDTO } from "@/domain/IOrderDTO";

export abstract class OrderApi {
    private static axios = Axios.create(
        {
            baseURL: "https://bodroz-food.azurewebsites.net/api/v1.0/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll () {
        return this.axios.get<IOrderDTO[]>('Orders', { headers: { Authorization: 'Bearer ' + localStorage.getItem('jwt') } }).then(response => {
            return response.data;
        })
    }

    static async confirmOrder(orderInfoDTO: IOrderInfoDTO) {
        return this.axios.post('UserPayings', orderInfoDTO, { headers: { Authorization: 'Bearer ' + localStorage.getItem('jwt') } })
    }
}
