import Axios from "axios";
import { IFoodCategoryDTO } from "@/domain/IFoodCategoryDTO";
import { IDrinkCategoryDTO } from "@/domain/IDrinkCategoryDTO";

export abstract class CategoryApi {
    private static axios = Axios.create(
        {
            baseURL: "https://bodroz-food.azurewebsites.net/api/v1.0/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAllDrinkCategory() {
        return this.axios.get<IFoodCategoryDTO[]>('DrinkCategories').then(response => {
            return response.data;
        })
    }

    static async getAllFoodCategory() {
        return this.axios.get<IDrinkCategoryDTO[]>('FoodCategories').then(response => {
            return response.data;
        })
    }
}
