import Axios from "axios";
import { IDeliveryDTO } from "@/domain/IDeliveryDTO";

export abstract class DeliveryApi {
    private static axios = Axios.create(
        {
            baseURL: "https://bodroz-food.azurewebsites.net/api/v1.0/Deliveries/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async takeOrder(deliveryId: string) {
        return this.axios.post<IDeliveryDTO[]>('', { deliveryId: deliveryId }, { headers: { Authorization: 'Bearer ' + localStorage.getItem('jwt') } }).then(response => {
            return response.data;
        })
    }

    static async getAll () {
        return this.axios.get<IDeliveryDTO[]>('', { headers: { Authorization: 'Bearer ' + localStorage.getItem('jwt') } }).then(response => {
            return response.data;
        })
    }

    static async getAllByUser (userId: string) {
        return this.axios.get<IDeliveryDTO[]>(userId, { headers: { Authorization: 'Bearer ' + localStorage.getItem('jwt') } }).then(response => {
            return response.data;
        })
    }
}
