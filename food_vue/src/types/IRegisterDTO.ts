export interface IRegisterDTO {
    Email: string,
    Password: string,
    FirstName: string,
    LastName: string,
    PhoneNumber: string,
    Birthday: Date
}
