export interface IReviewDTO {
    id: string;
    comment: string;
    rating: number;
    restaurantId: string;
    createdAt: Date;
    userName: string;
}
