import {IFoodItemDTO} from "@/domain/IFoodItem";
import {IDrinkItemDTO} from "@/domain/IDrinkItemDTO";

export interface IOrderDTO{
    orderNumber: string,
    restaurantName: string,
    orderPrice: string,
    status: string,
    foods: IFoodItemDTO[],
    drinks: IDrinkItemDTO[],
    date: Date
}
