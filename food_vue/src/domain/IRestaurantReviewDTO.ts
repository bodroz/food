import {IReviewDTO} from "@/domain/IReviewDTO";

export interface IRestaurantReviewDTO {
    id: string;
    name: string;
    description: string;
    reviewCount:string;
    rating: number;
    image: string,
    reviews: IReviewDTO[];
}
