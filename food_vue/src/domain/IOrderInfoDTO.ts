import {IDeliveryCreateDTO} from "@/domain/IDeliveryCreateDTO";
import {IRestaurantDTO} from "@/domain/IRestaurantDTO";
import {IFoodItemDTO} from "@/domain/IFoodItem";
import {IDrinkItemDTO} from "@/domain/IDrinkItemDTO";

export interface IOrderInfoDTO{
    Delivery: IDeliveryCreateDTO,
    Restaurant: IRestaurantDTO,
    price: string,
    FoodItems: IFoodItemDTO[],
    DrinkItems: IDrinkItemDTO[]
}
