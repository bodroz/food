import {IDeliveryCreateDTO} from "@/domain/IDeliveryCreateDTO";
import {IDeliveryItemDTO} from "@/domain/IDeliveryItemDTO";

export interface IDeliveryDTO extends IDeliveryCreateDTO{
    id: string,
    restaurantName: string,
    restaurantTelephone: string,
    restaurantAddress: string,
    restaurantPhone: string,
    userPhone: string,
    userName: string,
    foodItems: IDeliveryItemDTO[]
    drinkItems: IDeliveryItemDTO[]
}
