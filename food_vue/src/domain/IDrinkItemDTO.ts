import {IDrinkDTO} from "@/domain/IDrinkDTO";

export interface IDrinkItemDTO{
    quantity : number,
    drink: IDrinkDTO
}
