import {IFoodCategoryDTO} from "@/domain/IFoodCategoryDTO";
import {IRestaurantDTO} from "@/domain/IRestaurantDTO";

export interface IFoodDTO{
    id: string;
    name: string,
    price: string,
    foodCategory: IFoodCategoryDTO
    restaurant: IRestaurantDTO
}
