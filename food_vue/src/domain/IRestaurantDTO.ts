import { IFoodDTO } from "@/domain/IFoodDTO";
import { IDrinkDTO } from "@/domain/IDrinkDTO";

export interface IRestaurantDTO {
    id: string;
    name: string;
    description: string;
    image: string,
    reviewCount:string;
    rating: number;
    foods: IFoodDTO[];
    drinks: IDrinkDTO[];
}
