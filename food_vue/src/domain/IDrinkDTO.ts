import {IDrinkCategoryDTO} from "@/domain/IDrinkCategoryDTO";

export interface IDrinkDTO{
    id: string;
    name: string,
    price: string,
    drinkCategory: IDrinkCategoryDTO
}
