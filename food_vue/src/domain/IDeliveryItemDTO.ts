export interface IDeliveryItemDTO {
    itemName: string,
    quantity: number
}
