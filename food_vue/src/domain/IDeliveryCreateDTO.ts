export interface IDeliveryCreateDTO{
    apartmentNumber: string;
    street: string;
    postcode: string;
}
