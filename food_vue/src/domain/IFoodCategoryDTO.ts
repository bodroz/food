export interface IFoodCategoryDTO{
    id: string;
    category: string;
}
