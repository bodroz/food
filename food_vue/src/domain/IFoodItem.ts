import {IFoodDTO} from "@/domain/IFoodDTO";

export interface IFoodItemDTO{
    quantity : number,
    food : IFoodDTO
}
