import {TwoPlayers} from "../wwwroot/Game/twoPlayers";
import {playerVsCpu} from "../wwwroot/Game/playerVsCpu";
const type = document.getElementById("type");
const gameType = document.getElementById("playerVsplayer");
const pve = document.getElementById("playerVsCPU");
const restrartButton = document.getElementById('restartButton');


const board = document.getElementById('board');
const winning = document.getElementById('winningMessage');
function start() {
    board!.classList.remove('hide');
    let game = new TwoPlayers();
    game.startGame()
}
function startSingle() {
    board!.classList.remove('hide');
    let game = new playerVsCpu();
    game.startGame()
}

pve!.onclick = () => startSingle();
gameType!.onclick = () => start();
restrartButton!.onclick = () => showMenu();

function showMenu() {
    winning!.classList.remove('show');
    type!.classList.remove("hide");
}