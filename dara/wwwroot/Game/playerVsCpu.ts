import {Player} from "../Players/Player";
import {Ai} from "../Players/Ai";

export class playerVsCpu {
    skipClass = document.getElementById('skipButton')
    skipButton = document.getElementById('skip')
    backButton = document.getElementById('back');
    type = document.getElementById("type");
    action = document.getElementById("action");
    playerMessage = document.querySelector('.player');
    winningMessage = document.querySelector('.data-winning-message-text');
    actionMessage = document.querySelector('.actionMessage');
    winning = document.getElementById('winningMessage');
    gameType = document.getElementById("type");
    board = document.getElementById('board');
    cellElements = document.querySelectorAll('.cell');
    dropPhase: number = 0;
    whoMakesTurn: string = "blue";
    player: Player = new Player("Player", "blue");
    ai: Ai = new Ai("Computer", "red");
    gameBoard = this.createBoard();
    startGame() {
        this.skipClass!.classList.remove('hide')
        this.board!.className = "board";
        this.skipButton!.classList.remove("hide")
        this.action!.classList.remove("hide");
        this.board!.classList.remove("hide");
        this.gameType!.classList.add("hide");
        this.backButton!.onclick = () => this.backToMenu();
        this.skipButton!.onclick = () => this.skipTurn();
        this.randomGeneratedBoard();
        this.gameType!.classList.add("hide");
        if (this.whoMakesTurn == this.ai.color){
            setTimeout(() => this.aiTurn(), 600);
            setTimeout(() => this.addShowPossibleEvent(), 500);
        }
        else {
            this.addShowPossibleEvent()
        }
    }
    skipTurn(){
        if (this.player.squares >= 3){
            setTimeout(() => this.aiTurn(), 500)
        }

    }
    createBoard() {
        let board: string[] = [];
        for (let i = 0; i < this.cellElements.length; i++) {
            board[i] = "";
        }
        return board;
    }

    randomGeneratedBoard() {
        let possibleSpots: number[] = Array.from(new Array(30).keys());
        while (this.dropPhase != 24) {
            const randomElement: number = possibleSpots[Math.floor(Math.random() * possibleSpots.length)];
            this.gameBoard[randomElement] = this.whoMakesTurn;
            possibleSpots = possibleSpots.filter(x => x != randomElement);
            this.whoMakesTurn = this.whoMakesTurn == this.player.color ? this.ai.color : this.player.color;
            this.dropPhase++;

        }
        this.cellElements.forEach(cell => {
            let cellId = this.findId(cell);
            if (this.gameBoard[cellId].length != 0) {
                cell.classList.add(this.gameBoard[cellId])
            }
        })
    }

    findId(cell: Element) {
        let index: number = 0;
        for (let i = 0; i < this.cellElements.length; i++) {
            if (this.cellElements[i] == cell) {
                index = i;
                break;
            }
        }
        return index;
    }

    findCellsWithPossible(board: string[], aiTurn: boolean): number[] {
        let color = this.player.color;
        if (aiTurn) {
            color = this.ai.color;
        }
        let cells: number[] = [];
        for (let i = 0; i < board.length; i++) {
            if (board[i] == color) {
                if (this.ai.findPossibleForAi(i, board).length > 0) {
                    cells.push(i)
                }
            }
        }
        return cells;
    }


    minimax(board: string[], depth: number, maximizingPlayer: boolean, alpha: number, beta: number, threes: number): number {
        if (this.canRemoveEnemy(this.ai.previousCell!, board, this.ai.color) && this.ai.previousCell != undefined) {
            return 50 * depth + 10;
        }
        if (this.canRemoveEnemy(this.ai.playerPrevious!, board, this.player.color) && this.ai.playerPrevious != undefined && this.ai.squares > 6) {
            return -50 * depth - 10;
        }
        if (depth == 0) {
            return 0;
        }
        let moves = this.findCellsWithPossible(board, maximizingPlayer);
        if (moves.length == 0){
            maximizingPlayer = !maximizingPlayer;
            moves = this.findCellsWithPossible(board, maximizingPlayer);
        }
        if (maximizingPlayer) {
            let score = -Infinity;
            for (let move of moves) {
                for (let posMove of this.ai.findPossibleForAi(move, board)) {
                    let boardCopy = board.slice(0, board.length);
                    boardCopy[move] = "";
                    boardCopy[posMove] = this.ai.color;
                    this.ai.previousCell = posMove;
                    this.ai.playerPrevious = undefined;
                    let newScore = this.minimax(boardCopy, depth - 1, false, alpha, beta, threes);
                    if (newScore > score) {
                        score = newScore;
                    }
                    alpha = Math.max(alpha, score);
                    if (alpha >= beta) {
                        return alpha;
                    }
                }
            }
            return score;
        } else {
            let score = Infinity;
            for (let move of moves) {
                for (let posMove of this.ai.findPossibleForAi(move, board)) {
                    let boardCopy = board.slice(0, board.length);
                    boardCopy[move] = "";
                    boardCopy[posMove] = this.player.color;
                    this.ai.playerPrevious = posMove;
                    this.ai.previousCell = undefined;

                    let newScore = this.minimax(boardCopy, depth - 1, true, alpha, beta, threes);
                    if (threes < 0 && newScore == -260) {
                        threes += newScore;
                    }
                    if (newScore == -260 && threes == Infinity) {
                        threes = newScore;
                    }
                    if (newScore < score) {
                        score = newScore;
                    }
                    if (threes < -260) {
                        score = threes;
                    }
                    beta = Math.min(beta, score);
                    if (alpha >= beta) {
                        return alpha;
                    }
                }
            }
            return score;
        }
    }


    canRemoveEnemy(element: number, board: string[], color: string) {
        return this.vertical(element, board, color) || this.horizontal(element, board, color);
    }

    vertical(element: number, board: string[], color: string) {
        let id = element;
        let vertical = [];
        for (let index = 1; index < 5; index++) {
            if (id + 6 * index <= 29 && board[id + 6 * index] == color) {
                vertical.push(board[id + 6 * index])
            } else {
                break
            }
        }
        for (let index = 0; index < 4; index++) {
            if (id - 6 * index >= 0 && board[id - 6 * index] == color) {
                vertical.push(board[id - 6 * index])
            } else {
                break
            }
        }
        return vertical.length == 3;
    }

    horizontal(element: number, board: string[], color: string) {
        let id = element;
        let horizontal = [];
        for (let index = 1; index < 5; index++) {
            if (Math.floor(id / 6) == Math.floor((id - index) / 6) && board[id - index] == color) {
                horizontal.push(board[id - index])
            } else {
                break
            }
        }
        for (let index = 0; index < 4; index++) {
            if (Math.floor(id / 6) == Math.floor((id + index) / 6) && board[id + index] == color) {
                horizontal.push(board[id + index])
            } else {
                break
            }
        }
        return horizontal.length == 3;
    }


    hidePossible(elements: NodeListOf<Element>) {
        elements.forEach(cell => {
            if (cell.classList.contains("green")) {
                cell.classList.remove("green");
                (cell as HTMLElement).onclick = () => {
                };
            }
            cell.classList.remove("choosen");
        });
    }

    showPossible(cell: Element, player: Player) {
        this.hidePossible(this.cellElements);
        this.findPossible(cell, this.cellElements, player);
        if (player.possibleMoves.length == 0) {

        }
        this.cellElements.forEach(cell => {
            if (player.possibleMoves.includes(cell)) {
                cell.classList.add("green");
                (cell as HTMLElement).onclick = () => this.makeMove(cell);
            }
        });
    }

    findPossible(event: Element, elements: NodeListOf<Element>, player: Player) {
        player.possibleMoves = [];
        player.previousCell = event;
        const id = this.findId(event);
        if (id - 6 >= 0 && elements[id - 6].className == 'cell') {
            player.possibleMoves.push(elements[id - 6])
        }
        if (id + 6 <= 29 && elements[id + 6].className == 'cell') {
            player.possibleMoves.push(elements[id + 6])
        }
        if (Math.floor(id / 6) == Math.floor((id + 1) / 6) && elements[id + 1].className == 'cell') {
            player.possibleMoves.push(elements[id + 1])
        }
        if (Math.floor(id / 6) == Math.floor((id - 1) / 6) && elements[id - 1].className == 'cell') {
            player.possibleMoves.push(elements[id - 1])
        }
        event.classList.add("choosen");
    }

    placeMark(cell: Element, color: string) {
        cell.classList.remove("green");
        cell.classList.add(color);
        this.gameBoard[this.findId(cell)] = color;
    }

    removeMark(cell: Element, color: string) {
        cell.classList.remove(color);
        this.gameBoard[this.findId(cell)] = "";
        this.player.previousCell = undefined;
    }

    makeMove(element: Element) {
        this.removeMark(this.player!.previousCell!, this.player.color!);
        this.placeMark(element, this.player!.color);
        this.hidePossible(this.cellElements);
        if (this.canRemoveEnemy(this.findId(element), this.gameBoard, this.player.color)) {
            this.addRemoveEvent();
            this.actionMessage!.innerHTML = "Remove enemy square!";
        } else {
            this.addShowPossibleEvent();
            this.actionMessage!.innerHTML = "Choose your move!";
            setTimeout(() => this.aiTurn(), 500)
        }

    }
    bestToRemove(){
        let score = -10;
        let currentMove: { move: number, prevMove: number } | undefined;
        let moves = this.findCellsWithPossible(this.gameBoard, false);
        if (moves.length == 0) {
            let enemy: Element[] = [];
            this.cellElements.forEach(cell => {
                if (cell.classList.contains(this.player.color)) {
                    enemy.push(cell);
                }
            });
            currentMove = {move: -500, prevMove:this.findId(enemy[Math.floor(Math.random() * enemy.length)])};
            return currentMove
        }
        for (let move of moves) {
            if (score == 50){
                break;
            }
            for (let posMove of this.ai.findPossibleForAi(move, this.gameBoard)) {
                let boardCopy = this.gameBoard.slice(0, this.gameBoard.length);
                boardCopy[move] = "";
                boardCopy[posMove] = this.player.color;
                this.ai.playerPrevious = posMove;
                this.ai.previousCell = undefined;
                if (this.canRemoveEnemy(this.ai.playerPrevious!, boardCopy, this.player.color) && this.ai.playerPrevious != undefined && this.ai.squares > 6) {
                    currentMove = {move: posMove, prevMove: move}
                    score = 50;
                    break;
                }
                else {
                    score = 0;
                    currentMove = {move: posMove, prevMove: move}
                }
            }
        }
        return currentMove;
    }



    bestMove(){
        let score = -Infinity;
        let currentMove: { move: number, prevMove: number } | undefined;
        let moves = this.findCellsWithPossible(this.gameBoard, true);
        for (let move of moves) {
            for (let posMove of this.ai.findPossibleForAi(move, this.gameBoard)) {
                let boardCopy = this.gameBoard.slice(0, this.gameBoard.length);
                boardCopy[move] = "";
                boardCopy[posMove] = this.ai.color;
                this.ai.previousCell = posMove;
                this.ai.playerPrevious = undefined;
                let newScore = this.minimax(boardCopy, 6, false, -Infinity, Infinity, Infinity);
                if (newScore > score) {
                    score = newScore;
                    currentMove = {move: posMove, prevMove: move}
                }
            }
        }
        console.log(score)
        return currentMove;
    }
    aiTurn() {
        let currentMove = this.bestMove();
        this.placeMark(this.cellElements.item(currentMove!.move), this.ai.color);
        this.removeMark(this.cellElements.item(currentMove!.prevMove), this.ai.color);
        if (this.canRemoveEnemy(currentMove!.move,this.gameBoard, this.ai.color)) {
            let el2 = this.cellElements.item(this.bestToRemove()!.prevMove)
            this.removeEnemy(el2, this.player.color);
            this.gameBoard[this.findId(el2)] = ""
        }
    }

    addShowPossibleEvent() {
        if (this.findAllPossibleMoves(this.player.color).length > 0) {
            this.cellElements.forEach(cell => {
                if (cell.classList.contains(this.player!.color)) {
                    (cell as HTMLElement).onclick = () => this.showPossible(cell, this.player);
                } else {
                    (cell as HTMLElement).onclick = () => {
                    };
                }
            });
        }
        this.playerMessage!.innerHTML = this.player!.name + " is making turn as " + this.player!.color + " squares,";
        this.actionMessage!.innerHTML = "Choose your move!";
    }

    addRemoveEvent() {
        this.cellElements.forEach(cell => {
            if (cell.classList.contains(this.ai.color)) {
                (cell as HTMLElement).onclick = () => this.removeEnemy(cell, this.ai.color!);
            } else {
                (cell as HTMLElement).onclick = () => {
                };
            }
        });
    }

    removeEnemy(cell: Element, color: string) {
        cell.classList.remove(color);
        if (this.ai.color == color) {
            this.ai.squares--;
        } else {
            this.player.squares--;
        }
        this.endGame();
        this.gameBoard[this.findId(cell)] = "";
        this.addShowPossibleEvent();
        if (color != this.player.color && this.player.squares > 2 && this.ai.squares > 2) {
            setTimeout(() => this.aiTurn(), 500);
        }
    }

    findAllPossibleMoves(color: string): Element[] {
        let allPossibleMoves: Element[] = [];
        this.cellElements.forEach(cell => {
            if (cell.classList.contains(color)) {
                let id = this.findId(cell);
                if (id - 6 >= 0 && this.cellElements.item(id - 6).className == 'cell') {
                    allPossibleMoves.push(this.cellElements.item(id - 6))
                }
                if (id + 6 <= 29 && this.cellElements.item(id + 6).className == 'cell') {
                    allPossibleMoves.push(this.cellElements.item(id + 6))
                }
                if (Math.floor(id / 6) == Math.floor((id + 1) / 6) && this.cellElements.item(id + 1).className == 'cell') {
                    allPossibleMoves.push(this.cellElements.item(id + 1))
                }
                if (Math.floor(id / 6) == Math.floor((id - 1) / 6) && this.cellElements.item(id - 1).className == 'cell') {
                    allPossibleMoves.push(this.cellElements.item(id - 1))
                }
            }
        });
        return allPossibleMoves;
    }
    clearBoard() {
        this.cellElements.forEach(cell => {
            (cell as HTMLElement).onclick = () => {
            };
            cell.className = "cell";
        })
    }

    backToMenu() {
        this.clearBoard();
        this.board!.classList.add('hide');
        this.type!.classList.remove("hide");
        this.action!.classList.add("hide");
        this.skipClass!.classList.add('hide');
    }

    endGame() {
        if (this.ai.squares < 3) {
            this.clearBoard();
            this.action!.classList.add("hide");
            this.backButton!.classList.add("hide");
            this.winningMessage!.innerHTML = this.player.name + " wins!";
            this.clearClasses()
        }
        if (this.player.squares < 3) {
            this.clearBoard();
            this.action!.classList.add("hide");
            this.winningMessage!.innerHTML = this.ai.name + " wins!";
            this.clearClasses()
        }
    }
    clearClasses(){
        this.skipButton!.classList.add("hide");
        this.winning?.classList.add('show');
        this.board!.classList.add('hide');
        this.skipClass!.classList.add('hide');
    }
}