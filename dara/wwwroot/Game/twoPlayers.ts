import {Player} from "../Players/Player";

export class TwoPlayers {
    playerMessage = document.querySelector('.player');
    winningMessage = document.querySelector('.data-winning-message-text');
    actionMessage = document.querySelector('.actionMessage');
    winning = document.getElementById('winningMessage');
    cellElements = document.querySelectorAll('.cell');
    gameType = document.getElementById("type");
    board = document.getElementById('board');
    backButton = document.getElementById('back');
    type = document.getElementById("type");
    action = document.getElementById("action");
    dropPhase: number = 0;
    playerWhichMakeTurn: Player | undefined;
    player1 = new Player("Player1", "blue");
    player2 = new Player("Player2", "red");

    startGame(): void {
        this.action!.classList.remove("hide");
        this.board!.classList.remove("hide");
        this.gameType!.classList.add("hide");
        this.backButton!.onclick = () => this.backToMenu();
        this.playerWhichMakeTurn = this.player1;
        this.playerMessage!.innerHTML = this.playerWhichMakeTurn.name + " is making turn as " + this.playerWhichMakeTurn.color + " squares,";
        this.actionMessage!.innerHTML = "Set your square!";
        this.cellElements.forEach(cell => {
            (cell as HTMLElement).onclick = () => this.dropPhaseEvent(cell, this.player1, this.player2);
        });
        this.setBoardHoverClass(this.player1, this.player2)
    }

    setBoardHoverClass(player1: Player, player2: Player): void {
        this.board!.classList.remove(player1.color);
        this.board!.classList.remove(player2.color);
        if (this.playerWhichMakeTurn === player1 && this.dropPhase < 23) {
            this.board!.classList.add(player1.color)
        } else if (this.dropPhase < 23) {
            this.board!.classList.add(player2.color)
        }
    }

    dropPhaseEvent(element: Element, player1: Player, player2: Player): void {
        if (this.dropPhase < 24) {
            this.placeMark(element, this.playerWhichMakeTurn!);
            this.swapTurns();
            this.setBoardHoverClass(player1, player2);
            this.dropPhase++;
            this.playerMessage!.innerHTML = this.playerWhichMakeTurn!.name + " is making turn as " + this.playerWhichMakeTurn!.color + " squares,";
            this.actionMessage!.innerHTML = "Set your square!";
            (element as HTMLElement).onclick = () => {
            };
            this.endOfDropPhase();
        }
    }

    endOfDropPhase() {
        if (this.dropPhase == 24) {
            this.addShowPossibleEvent();
            this.playerMessage!.innerHTML = this.playerWhichMakeTurn!.name + " is making turn as " + this.playerWhichMakeTurn!.color + " squares,";
            this.actionMessage!.innerHTML = "Choose your move!";
        }
    }

    swapTurns() {
        let previousPlayer = this.playerWhichMakeTurn;
        this.playerWhichMakeTurn = previousPlayer!.color == "blue" ? this.player2 : this.player1;
    }

    placeMark(cell: Element, player: Player) {
        cell.classList.remove("green");
        cell.classList.add(player.color)
    }


    hidePossible(elements: NodeListOf<Element>) {
        elements.forEach(cell => {
            if (cell.classList.contains("green")) {
                cell.classList.remove("green");
                (cell as HTMLElement).onclick = () => {
                };
            }
            cell.classList.remove("choosen");
        });
    }

    showPossible(cell: Element, player: Player) {
        this.hidePossible(this.cellElements);
        this.findPossible(cell, this.cellElements, player);
        this.cellElements.forEach(cell => {
            if (player.possibleMoves.includes(cell)) {
                cell.classList.add("green");
                (cell as HTMLElement).onclick = () => this.makeMove(cell);
            }
        });
        this.playerMessage!.innerHTML = this.playerWhichMakeTurn!.name + " is making turn as " + this.playerWhichMakeTurn!.color + " squares,";
        this.actionMessage!.innerHTML = "Choose your move!";
    }

    findPossible(event: Element, elements: NodeListOf<Element>, player: Player) {
        player.possibleMoves = [];
        player.previousCell = event;
        const id = this.findId(event);
        if (id - 6 >= 0 && elements[id - 6].className == 'cell') {
            player.possibleMoves.push(elements[id - 6])
        }
        if (id + 6 <= 29 && elements[id + 6].className == 'cell') {
            player.possibleMoves.push(elements[id + 6])
        }
        if (Math.floor(id / 6) == Math.floor((id + 1) / 6) && elements[id + 1].className == 'cell') {
            player.possibleMoves.push(elements[id + 1])
        }
        if (Math.floor(id / 6) == Math.floor((id - 1) / 6) && elements[id - 1].className == 'cell') {
            player.possibleMoves.push(elements[id - 1])
        }
        event.classList.add("choosen")
    }

    makeMove(element: Element) {
        this.removeMark(this.playerWhichMakeTurn!.previousCell!, this.playerWhichMakeTurn!);
        this.placeMark(element, this.playerWhichMakeTurn!);
        this.hidePossible(this.cellElements);
        this.swapTurns();
        if (this.canRemoveEnemy(element)) {
            this.addRemoveEvent();
            this.actionMessage!.innerHTML = "Remove enemy square!";
        } else {
            this.addShowPossibleEvent();
            this.playerMessage!.innerHTML = this.playerWhichMakeTurn!.name + " is making turn as " + this.playerWhichMakeTurn!.color + " squares,";
            this.actionMessage!.innerHTML = "Choose your move!";
        }
    }

    addShowPossibleEvent() {
        this.findAllPossibleMoves();
        if (this.playerWhichMakeTurn!.allPossibleMoves.length == 0) {
            this.swapTurns();
        }
        this.cellElements.forEach(cell => {
            if (cell.classList.contains(this.playerWhichMakeTurn!.color)) {
                (cell as HTMLElement).onclick = () => this.showPossible(cell, this.playerWhichMakeTurn!);
            } else {
                (cell as HTMLElement).onclick = () => {
                };
            }
        });
    }

    addRemoveEvent() {
        this.cellElements.forEach(cell => {
            if (cell.classList.contains(this.playerWhichMakeTurn!.color)) {
                (cell as HTMLElement).onclick = () => this.removeEnemy(cell, this.playerWhichMakeTurn!);
            } else {
                (cell as HTMLElement).onclick = () => {
                };
            }
        });
    }

    removeMark(cell: Element, player: Player) {
        cell.classList.remove(player.color);
        player.previousCell = undefined;
    }

    findId(cell: Element) {
        let index: number = 0;
        for (let i = 0; i < this.cellElements.length; i++) {
            if (this.cellElements[i] == cell) {
                index = i;
                break;
            }
        }
        return index;
    }

    findAllPossibleMoves() {
        this.playerWhichMakeTurn!.allPossibleMoves = [];
        this.cellElements.forEach(cell => {
            if (cell.classList.contains(this.playerWhichMakeTurn!.color)) {
                let id = this.findId(cell);
                if (id - 6 >= 0 && this.cellElements.item(id - 6).className == 'cell') {
                    this.playerWhichMakeTurn!.allPossibleMoves.push(this.cellElements.item(id - 6))
                }
                if (id + 6 <= 29 && this.cellElements.item(id + 6).className == 'cell') {
                    this.playerWhichMakeTurn!.allPossibleMoves.push(this.cellElements.item(id + 6))
                }
                if (Math.floor(id / 6) == Math.floor((id + 1) / 6) && this.cellElements.item(id + 1).className == 'cell') {
                    this.playerWhichMakeTurn!.allPossibleMoves.push(this.cellElements.item(id + 1))
                }
                if (Math.floor(id / 6) == Math.floor((id - 1) / 6) && this.cellElements.item(id - 1).className == 'cell') {
                    this.playerWhichMakeTurn!.allPossibleMoves.push(this.cellElements.item(id - 1))
                }
            }
        });
    }

    removeEnemy(cell: Element, player: Player) {
        cell.classList.remove(player.color);
        player.squares--;
        this.endGame();
        this.addShowPossibleEvent();
        this.playerMessage!.innerHTML = this.playerWhichMakeTurn!.name + " is making turn as " + this.playerWhichMakeTurn!.color + " squares,";
        this.actionMessage!.innerHTML = "Choose your move!";
    }

    vertical(element: Element) {
        let id = this.findId(element);
        let vertical = [];
        for (let index = 1; index < 5; index++) {
            if (id + 6 * index <= 29 && this.cellElements.item(id + 6 * index).className == element.className) {
                vertical.push(this.cellElements.item(id + 6 * index))
            } else {
                break
            }
        }
        for (let index = 0; index < 4; index++) {
            if (id - 6 * index >= 0 && this.cellElements.item(id - 6 * index).className == element.className) {
                vertical.push(this.cellElements.item(id - 6 * index))
            } else {
                break
            }
        }
        return vertical.length == 3;
    }

    horizontal(element: Element) {
        let id = this.findId(element);
        let horizontal = [];
        for (let index = 1; index < 5; index++) {
            if (Math.floor(id / 6) == Math.floor((id - index) / 6) && this.cellElements.item(id - index).className == element.className) {
                horizontal.push(this.cellElements.item(id - index))
            } else {
                break
            }
        }
        for (let index = 0; index < 4; index++) {
            if (Math.floor(id / 6) == Math.floor((id + index) / 6) && this.cellElements.item(id + index).className == element.className) {
                horizontal.push(this.cellElements.item(id + index))
            } else {
                break
            }
        }

        return horizontal.length == 3;
    }

    canRemoveEnemy(element: Element) {
        return this.vertical(element) || this.horizontal(element)
    }

    clearBoard() {
        this.cellElements.forEach(cell => {
            (cell as HTMLElement).onclick = () => {
            };
            cell.className = "cell";
        })
    }

    backToMenu() {
        this.clearBoard();
        this.board!.classList.add('hide');
        this.type!.classList.remove("hide");
        this.action!.classList.add("hide");
    }

    endGame() {
        if (this.player2.squares < 3) {
            this.clearBoard();
            this.action!.classList.add("hide");
            this.backButton!.classList.add("hide");
            this.winningMessage!.innerHTML = this.player1.name + " wins!";
            this.winning?.classList.add('show');
            this.board!.classList.add('hide');

        }
        if (this.player1.squares < 3) {
            this.clearBoard();
            this.action!.classList.add("hide");
            this.backButton!.classList.add("hide");
            this.winningMessage!.innerHTML = this.player2.name + " wins!";
            this.winning?.classList.add('show');
            this.board!.classList.add('hide');
        }
    }
}