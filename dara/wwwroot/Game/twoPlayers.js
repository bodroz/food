"use strict";
exports.__esModule = true;
var Player_1 = require("../Player/Player");
var Game = /** @class */ (function () {
    function Game() {
        this.dropPhaes = 0;
        this.cellElements = document.querySelectorAll('.cell');
        this.board = document.getElementById('board');
    }
    Game.prototype.startGame = function () {
        var _this = this;
        var player1 = new Player_1.Player("Player1", "blue");
        var player2 = new Player_1.Player("Player2", "red");
        this.makeTurn = player1;
        this.cellElements.forEach(function (cell) {
            cell.addEventListener('click', function () { return _this.dropPhase; }, { once: true });
        });
        this.setBoardHoverClass(player1, player2);
    };
    Game.prototype.setBoardHoverClass = function (player1, player2) {
        this.board.classList.remove(player1.color);
        this.board.classList.remove(player2.color);
        if (this.makeTurn === player1 && this.dropPhaes < 23) {
            this.board.classList.add(player1.color);
        }
        else if (this.dropPhaes < 23) {
            this.board.classList.add(player2.color);
        }
    };
    Game.prototype.dropPhase = function (event, player1, player2) {
        var _this = this;
        if (this.dropPhaes < 24) {
            this.makeTurn.placeMark(event.target);
            this.swapTurns(player1, player2);
            this.setBoardHoverClass(player1, player2);
            this.dropPhaes++;
            if (this.dropPhaes == 24) {
                this.cellElements.forEach(function (cell) {
                    cell.removeEventListener('click', function () { return _this.dropPhase; });
                });
            }
        }
    };
    Game.prototype.swapTurns = function (player1, player2) {
        this.makeTurn = this.makeTurn.color == "blue" ? player1 : player2;
    };
    return Game;
}());
exports.Game = Game;
