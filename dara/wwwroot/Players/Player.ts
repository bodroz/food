export class Player {

    name: string;
    squares: number = 12;
    color: string = "";
    possibleMoves : Element[] = [];
    allPossibleMoves: Element[] = [];
    previousCell : Element | undefined;
    constructor(playerName: string, color: string) {
        this.name = playerName;
        this.color = color;
    }
}