"use strict";
exports.__esModule = true;
var Player = /** @class */ (function () {
    function Player(playerName, color) {
        this.squares = 12;
        this.color = "";
        this.name = playerName;
        this.color = color;
    }
    Player.prototype.placeMark = function (cell) {
        cell.classList.remove("green");
        cell.classList.add(this.color);
    };
    return Player;
}());
exports.Player = Player;
