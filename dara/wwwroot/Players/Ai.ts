export class Ai {

    name: string;
    squares: number = 12;
    color: string = "";
    firstMove: boolean = false;
    possibleMoves : Element[] = [];
    allPossibleMoves: Element[] = [];
    previousCell : number | undefined;
    playerPrevious: number | undefined;
    constructor(playerName: string, color: string) {
        this.name = playerName;
        this.color = color;

    }
    findPossibleForAi(element: number, board: string[]) {
        let possibleMoves: number[] = [];
        const id = element;
        if (id - 6 >= 0 && board[id - 6] == "") {
            possibleMoves.push(id - 6)
        }
        if (id + 6 <= 29 && board[id + 6] == "") {
            possibleMoves.push(id + 6)
        }
        if (Math.floor(id / 6) == Math.floor((id + 1) / 6) && board[id + 1] == "") {
            possibleMoves.push(id + 1)
        }
        if (Math.floor(id / 6) == Math.floor((id - 1) / 6) && board[id - 1] == "") {
            possibleMoves.push(id - 1)
        }
        return possibleMoves;
    }
}